<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My Phone Book</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  </head>
  <body>
    <div class="" id="app">
      <MyHeader></MyHeader>

        <router-view></router-view>
        

      <myfooter></myfooter>
    </div>

  </body>

  <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</html>
